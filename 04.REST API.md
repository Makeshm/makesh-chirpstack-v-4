### REST API 
***

 With the introduction of ChirpStack v4, the REST API interface is no longer included. Historically it was included to serve the web-interface, as at that time, gRPC could not be used within the browser. The included REST interface internally translated REST requests into gRPC and back. The ChirpStack gRPC to REST API proxy is like a bridge that lets you use ChirpStack (v4) in two different ways: gRPC and REST API. The gRPC way is usually suggested, but sometimes people find it easier to use the REST API. This can be especially helpful when moving from ChirpStack v3 to v4 because, in the past, this proxy used to be a part of the ChirpStack Application Server."

#### Usage
***

1. __Install package__

```
sudo apt install chirpstack-rest-api
```

2. __Configuration__

* Environment variables can be used to configure the ChirpStack REST API proxy. You will find this configuration in /etc/chirpstack-rest-api/environment.

* __(Re)start and stop__

```
sudo systemctl [restart|start|stop] chirpstack-rest-api
```

